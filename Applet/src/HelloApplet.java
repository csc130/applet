import java.awt.*;

import javax.swing.JApplet;

import java.util.Scanner;
public class HelloApplet extends JApplet {
	public void paint(Graphics g) {
		//variable from primitive data types
		int x=10;
		int y=100;
		Font font = new Font("Arial",Font.BOLD,30);
	    g.setFont(font);
		
		//instance from class
		//INSTANTIATION: an action to create an instance
		//keyboard is an object of Scanner class
		//when you want to provide the value to instance, we need to invoke a constructor
		//CONSTUCTOR: a method that has the same name as your class name, 
		//which provides an initial values to the instance
		Scanner keyboard = new Scanner(System.in);
		
		String name = "Tanes";
		String lastname = new String("Kanchanawanchai");
		
		g.drawString(name.charAt(0)+"", x+10, y+100);
		
		g.drawString(name, x, y);
		
	}
}
